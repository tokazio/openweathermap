# OpenWeatherMap

Prends les données de OpenWeatherMap et les mets dans une base H2

Declares itself as a zeroconf/bonjour service at application start.

Putting info in the database every 30 minutes (see application.properties to change it)

You must provide your OpenWeatherMap key and lat/lon of yout location in the application.properties

## Service compteur

```
sudo mkdir /opt/openweathermap
```

```
cd /home/odroid/openweathermap
```

```
gradle build -Dquarkus.package.type=uber-jar -x test
```

```
sudo cp /home/odroid/openweathermap/build/openweathermap-1.0-SNAPSHOT-runner.jar /opt/openweathermap/openweathermap.jar
```

sudo nano /etc/systemd/system/openweathermap.service

```
[Unit]
Description=OpenWeatherMap Java service

[Service]
WorkingDirectory=/opt/openweathermap
ExecStart=/usr/bin/java -Xmx128m -server -jar openweathermap.jar >> /opt/openweathermap/openweathermap.log
User=root
Type=simple
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
```

```
sudo systemctl daemon-reload
```

```
sudo systemctl enable openweathermap
```

```
sudo mkdir /opt/openweathermap/config
```

```
sudo nano /opt/openweathermap/config/application.properties
```

Put your apikey, lat and lon in this configuration file

## Changing configuration

create a folder /opt/openweathermap/config with an application.properties file in it to override the embedded one.

```
nano src/main/resources/application.properties
gradle build -Dquarkus.package.type=uber-jar -x test
java -jar build/openweathermap-1.0-SNAPSHOT-runner.jar
```

## faire le odroid resize.sh

```
wget -O /usr/local/bin/odroid-utility.sh https://raw.githubusercontent.com/mdrjr/odroid-utility/master/odroid-utility.sh
chmod +x /usr/local/bin/odroid-utility.sh /usr/local/bin/odroid-utility.sh
```

## Packaging and running the application

The application can be packaged using:

```shell script
./gradlew build
```

It produces the `quarkus-run.jar` file in the `build/quarkus-app/` directory. Be aware that it’s not an _über-jar_ as
the dependencies are copied into the `build/quarkus-app/lib/` directory.

The application is now runnable using `java -jar build/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:

```shell script
./gradlew build -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar build/*-runner.jar`.
