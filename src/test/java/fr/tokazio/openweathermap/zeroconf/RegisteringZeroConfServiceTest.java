package fr.tokazio.openweathermap.zeroconf;

import org.junit.jupiter.api.Test;

import java.net.SocketException;

class RegisteringZeroConfServiceTest {

    @Test
    void allIps() throws SocketException {
        //given
        final RegisteringZeroConfService service = new RegisteringZeroConfService();

        //when
        final String str = service.allIps();

        //then
        System.out.println(str);
    }
}
