package fr.tokazio.openweathermap;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Entity
@Cacheable
public class WeatherEntity {

    @Id
    private final String id = UUID.randomUUID().toString();
    private BigDecimal temp;
    private Integer pressure;
    private Integer humidiy;
    private String weatherMain;
    private String weatherDesc;
    private BigDecimal windSpeed;
    private int windDeg;
    private BigDecimal windGust;
    private Date date;

    WeatherEntity() {
        super();
    }

    public WeatherEntity(
            final BigDecimal temp,
            final int pressure,
            final int humidity,
            final String weatherMain,
            final String weatherDesc,
            final BigDecimal windSpeed,
            final int windDeg,
            final BigDecimal windGust,
            final Date dt
    ) {
        this.temp = temp;
        this.pressure = pressure;
        this.humidiy = humidity;
        this.weatherMain = weatherMain;
        this.weatherDesc = weatherDesc;
        this.windSpeed = windSpeed;
        this.windDeg = windDeg;
        this.windGust = windGust;
        this.date = dt;
    }

    public String getId() {
        return id;
    }

    public BigDecimal getTemp() {
        return temp;
    }

    public Integer getPressure() {
        return pressure;
    }

    public Integer getHumidiy() {
        return humidiy;
    }

    public String getWeatherMain() {
        return weatherMain;
    }

    public String getWeatherDesc() {
        return weatherDesc;
    }

    public BigDecimal getWindSpeed() {
        return windSpeed;
    }

    public int getWindDeg() {
        return windDeg;
    }

    public BigDecimal getWindGust() {
        return windGust;
    }

    public Date getDate() {
        return date;
    }


}
