package fr.tokazio.openweathermap;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class OpenWeatherMapRepositoryImpl implements OpenWeatherMapRepository {

    @Inject
    EntityManager em;

    @Override
    public List<WeatherEntity> all(final String from, final String to) {
        if (from == null && to == null) {
            return em.createQuery("from WeatherEntity order by date desc", WeatherEntity.class)
                    .getResultList();
        } else if (from != null && to == null) {
            return em.createQuery("from WeatherEntity where date >= :from order by date desc", WeatherEntity.class)
                    .setParameter("from", Utils.asDate(from))
                    .getResultList();
        } else if (from == null) {
            return em.createQuery("from WeatherEntity where date <= :to order by date desc", WeatherEntity.class)
                    .setParameter("to", Utils.asDate(to))
                    .getResultList();
        }
        return em.createQuery("from WeatherEntity where date >= :from and date <= :to order by date desc", WeatherEntity.class)
                .setParameter("from", Utils.asDate(from))
                .setParameter("to", Utils.asDate(to))
                .getResultList();
    }

    @Transactional
    @Override
    public void save(final WeatherData data) {
        em.persist(new WeatherEntity(
                        data.getTemp(),
                        data.getPressure(),
                        data.getHumidiy(),
                        data.getWeatherMain(),
                        data.getWeatherDesc(),
                        data.getWindSpeed(),
                        data.getWindDeg(),
                        data.getWindGust(),
                        data.getDate()
                )
        );
    }
}
