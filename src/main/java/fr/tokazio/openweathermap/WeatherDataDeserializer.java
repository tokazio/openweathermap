package fr.tokazio.openweathermap;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;
import java.util.Iterator;

@ApplicationScoped
public class WeatherDataDeserializer extends StdDeserializer<WeatherData> {

    @Inject
    Logger log;

    public WeatherDataDeserializer() {
        this(null);
    }

    public WeatherDataDeserializer(final Class<?> vc) {
        super(vc);
    }

    @Override
    public WeatherData deserialize(final JsonParser jp, final DeserializationContext ctxt)
            throws IOException {
        final JsonNode node = jp.getCodec().readTree(jp);

        final BigDecimal temp = node.get("main").get("temp").decimalValue();
        final int pressure = node.get("main").get("pressure").asInt();
        final int humidity = node.get("main").get("humidity").asInt();

        String weatherMain = "";
        String weatherDesc = "";

        final ArrayNode arrayNode = (ArrayNode) node.get("weather");
        final Iterator<JsonNode> itr = arrayNode.elements();
        int nb = 0;
        while (itr.hasNext()) {
            final JsonNode n = itr.next();
            weatherMain = n.get("main").asText();
            weatherDesc = n.get("description").asText();
            nb += 1;
            if (nb > 1) {
                break;
            }
        }
        if (nb > 1) {
            log.warn("Multiple (" + nb + ") weather definitions");
        }
        final BigDecimal windSpeed = node.get("wind").get("speed").decimalValue();
        final int windDeg = node.get("wind").get("deg").asInt();
        final BigDecimal windGust = node.get("wind").get("gust").decimalValue();

        final long dt = node.get("dt").asLong();

        final Instant instant = Instant.ofEpochSecond(dt);

        return new WeatherData(
                temp,
                pressure,
                humidity,
                weatherMain,
                weatherDesc,
                windSpeed,
                windDeg,
                windGust,
                Date.from(instant)
        );
    }

}