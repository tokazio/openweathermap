package fr.tokazio.openweathermap;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class OpenWeatherMapService {

    @ConfigProperty(name = "apikey")
    String apikey;

    @ConfigProperty(name = "lat")
    float lat;

    @ConfigProperty(name = "lon")
    float lon;

    @Inject
    @RestClient
    OpenWeatherMapClient client;

    public WeatherData getFromAPI() {
        return client.getData(lat, lon, "metric", apikey);
    }
}
