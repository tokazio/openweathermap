package fr.tokazio.openweathermap;

import io.quarkus.scheduler.Scheduled;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class OpenWeatherMapConsumer {

    @Inject
    Logger log;

    @Inject
    OpenWeatherMapService service;

    @Inject
    OpenWeatherMapRepository repository;

    @Scheduled(every = "{every-weather}")
    public void consume() {
        final WeatherData data = service.getFromAPI();
        log.info("Got " + data);
        repository.save(data);
    }

}
