package fr.tokazio.openweathermap;

import io.quarkus.cache.CacheResult;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/weather")
@Produces(MediaType.APPLICATION_JSON)
public class WeatherResource {

    @Inject
    OpenWeatherMapRepository repository;

    @GET
    @CacheResult(cacheName = "openwheathermap-cache")
    public List<WeatherEntity> all(@QueryParam("from") String from, @QueryParam("to") String to) {
        return repository.all(from, to);
    }
}
