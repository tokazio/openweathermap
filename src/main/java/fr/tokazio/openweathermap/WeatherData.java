package fr.tokazio.openweathermap;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

//https://openweathermap.org/current
@JsonDeserialize(using = WeatherDataDeserializer.class)
public class WeatherData {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");


    private final BigDecimal temp;//°C
    private final Integer pressure;//hPa
    private final Integer humidiy;//%
    private final String weatherMain;
    private final String weatherDesc;
    private final BigDecimal windSpeed;//m/sec
    private final int windDeg;
    private final BigDecimal windGust; //m/sec
    private final Date date; //unix UTC

    public WeatherData(
            final BigDecimal temp,
            final int pressure,
            final int humidity,
            final String weatherMain,
            final String weatherDesc,
            final BigDecimal windSpeed,
            final int windDeg,
            final BigDecimal windGust,
            final Date dt
    ) {
        this.temp = temp;
        this.pressure = pressure;
        this.humidiy = humidity;
        this.weatherMain = weatherMain;
        this.weatherDesc = weatherDesc;
        this.windSpeed = windSpeed;
        this.windDeg = windDeg;
        this.windGust = windGust;
        this.date = dt;
    }


    public BigDecimal getTemp() {
        return temp;
    }

    public Integer getPressure() {
        return pressure;
    }

    public Integer getHumidiy() {
        return humidiy;
    }

    public String getWeatherMain() {
        return weatherMain;
    }

    public String getWeatherDesc() {
        return weatherDesc;
    }

    public BigDecimal getWindSpeed() {
        return windSpeed;
    }

    public int getWindDeg() {
        return windDeg;
    }

    public BigDecimal getWindGust() {
        return windGust;
    }

    public Date getDate() {
        return date;
    }

    public String getDateAsString() {
        return DATE_FORMAT.format(this.date);
    }

    @Override
    public String toString() {
        return "WeatherData{" +
                "temp=" + temp +
                ", pressure=" + pressure +
                ", humidiy=" + humidiy +
                ", weatherMain='" + weatherMain + '\'' +
                ", weatherDesc='" + weatherDesc + '\'' +
                ", windSpeed=" + windSpeed +
                ", windDeg=" + windDeg +
                ", windGust=" + windGust +
                ", date=" + date +
                '}';
    }

}