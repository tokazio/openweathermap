package fr.tokazio.openweathermap;

import java.util.List;

public interface OpenWeatherMapRepository {

    List<WeatherEntity> all(String from, String to);

    void save(WeatherData data);
}
