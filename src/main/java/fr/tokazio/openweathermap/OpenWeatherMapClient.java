package fr.tokazio.openweathermap;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;

@Path("/weather")
@RegisterRestClient(configKey = "openweathermap-client")
@Consumes("application/json")
@Produces("application/json")
public interface OpenWeatherMapClient {

    @GET
    WeatherData getData(@QueryParam("lat") float lat, @QueryParam("lon") float lon, @QueryParam("units") String units, @QueryParam("appid") String appid);

}
